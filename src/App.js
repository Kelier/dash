import React, {useRef, useEffect} from 'react';
import * as THREE from 'three';
import * as Stats from 'stats-js';
import './App.css';

import {OBJLoader} from 'three/examples/jsm/loaders/OBJLoader';
import {MTLLoader} from 'three/examples/jsm/loaders/MTLLoader';
import {OrbitControls} from 'three/examples/jsm/controls/OrbitControls';

import faceMtl from './model/00150_mesh_fnum19_simed_statistical_removed_upsampled_pointcloud_mced_clean_nonManifold_neck_cutted_holesmoothed_origin_uv.mtl';
import faceObj from './model/00150_mesh_fnum19_simed_statistical_removed_upsampled_pointcloud_mced_clean_nonManifold_neck_cutted_holesmoothed_origin_uv.obj';
import faceJpg from './model/00150_mesh_fnum19_simed_statistical_removed_upsampled_pointcloud_mced_clean_nonManifold_neck_cutted_holesmoothed_origin_uv.jpg';

const renderView = (view) => {
    const width = window.innerWidth;
    const height = window.innerHeight;

    // todo 初始化場景
    const scene = new THREE.Scene();
    scene.background = new THREE.Color('black');

    // todo 載入相機
    const camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 1000);  // 远景相机
    camera.aspect = width / height;
    camera.position.set(0, 1, 10);

    //todo 載入光線
    // const ambLight = new THREE.AmbientLight(0x404040, 0.5);
    // const pointLight = new THREE.PointLight(0x404040, 0.8);
    const directionalLight = new THREE.DirectionalLight(0xffffff, 0.5);
    directionalLight.position.set(200, 200, 1000).normalize();
    // pointLight.receiveShadow = true;
    directionalLight.castShadow = true;
    // scene.add(ambLight);
    // scene.add(pointLight);
    scene.add(directionalLight);
    scene.add(directionalLight.target);

    //todo renderer
    const renderer = new THREE.WebGLRenderer({
        antialias: true,
        alpha: true
    });
    renderer.setSize(width, height);
    //renderer.setClearColor(0xb9d3ff,1);
    // renderer.setClearColor(0x000000, 1.0);
    renderer.shadowMapEnabled = true;

    //todo  載入模型model
    let mtlLoader = new MTLLoader();
    mtlLoader.load(faceMtl,
        function (materials) {
            console.log('set mtl', materials)
            materials.preload();
            let objLoader = new OBJLoader();
            objLoader.setMaterials(materials);
            objLoader.load(faceObj, function (object) {
                console.log('set obj', object);
                for (let i = 0; i < object.children.length; i++) {
                    let material = object.children[i].material;
                    let geometry = object.children[i].geometry;
                    let textureLoader = new THREE.TextureLoader();
                    textureLoader.load(faceJpg, function (texture) {
                        material.map = texture;
                        // add mesh to scene:
                        let meshObj = new THREE.Mesh(geometry, material);
                        meshObj.receiveShadow = true;
                        meshObj.castShadow = true;
                        scene.add(meshObj);
                        renderer.render(scene, camera);//执行渲染操作
                    });
                }
            });
        }
    );

    function render() {
        renderer.render(scene, camera);//执行渲染操作
    }
    let controls = new OrbitControls(camera, renderer.domElement);
    // 如果使用animate方法时，将此函数删除
    controls.addEventListener('change', render);
    // 使动画循环使用时阻尼或自转 意思是否有惯性
    controls.enableDamping = true;
    //动态阻尼系数 就是鼠标拖拽旋转灵敏度
    //controls.dampingFactor = 0.25;
    //是否可以缩放
    controls.enableZoom = true;
    // //是否自动旋转
    // controls.autoRotate = true;
    // //设置相机距离原点的最远距离
    // controls.minDistance = 50;
    // //设置相机距离原点的最远距离
    // controls.maxDistance = 200;
    //是否开启右键拖拽
    controls.enablePan = true;

    // 创建性能面板
    const stats = new Stats();
    stats.showPanel(0);

    view.appendChild(renderer.domElement);
    view.appendChild(stats.dom);

    function animate() {
        stats.begin();
        // camera.position.z = camera.position.z - .01; // 接受正义的审判吧！
        renderer.render(scene, camera); //执行渲染操作
        stats.end();
        requestAnimationFrame(animate);
    }

    requestAnimationFrame(animate);
}

function App() {
    const view = useRef(null);
    useEffect(() => {
        console.log(view.current, 'view')
        renderView(view.current);
    });
    return (
        <div className="App">
            <div ref={view}></div>
        </div>
    );
}

export default App;
